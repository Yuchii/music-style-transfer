# Music Style Transfer
## What this is

This is a repository for the final project of 10-707 (Topics in Deep Learning) at CMU. Based on the popular [style transfer paper](https://www.cv-foundation.org/openaccess/content_cvpr_2016/papers/Gatys_Image_Style_Transfer_CVPR_2016_paper.pdf), we investigated the applicability of using this technique on sequential data (specifically music). 

## Our approach
Our approach leverages the reconstruction abilities of an autoencoder in lieu of a discriminative network (like in the original paper) to extract the relevant low and high level features of music. In particular, we are interested in the high-level structure of music, such as tempo or rhythm, and not low-level details such as timbre. As a result, we do not use raw sound as input but rather processed midi files. This has the additional benefit of significantly speeding up the learning process. During generation, we feed in low-level features of a **style** song and high level features of a **content** song to the encoder and the decoder generates the style-fused piece. This is different to the original paper where the authors use backpropogration to minimize the high and low level differences between the original picture and the new picture.

## Requirements
TensorFlow and pretty_midi are required to run. Install requirements.txt to do so.