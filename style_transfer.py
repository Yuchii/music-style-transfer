# Music Style Transfer Project
# Author: Yuchi Wang
import os
import numpy as np
import tensorflow as tf
import pretty_midi
import sys

# define parameters
epochs = 50
epochsnn = 50
batches = 100
np.random.seed()
def piano_roll_to_pretty_midi(piano_roll, fs=100, program=0):
    '''Convert a Piano Roll array into a PrettyMidi object
     with a single instrument.

    Parameters
    ----------
    piano_roll : np.ndarray, shape=(128,frames), dtype=int
        Piano roll of one instrument
    fs : int
        Sampling frequency of the columns, i.e. each column is spaced apart
        by ``1./fs`` seconds.
    program : int
        The program number of the instrument.

    Returns
    -------
    midi_object : pretty_midi.PrettyMIDI
        A pretty_midi.PrettyMIDI class instance describing
        the piano roll.

    '''
    notes, frames = piano_roll.shape
    pm = pretty_midi.PrettyMIDI()
    instrument = pretty_midi.Instrument(program=program)

    # pad 1 column of zeros so we can acknowledge inital and ending events
    piano_roll = np.pad(piano_roll, [(0, 0), (1, 1)], 'constant')

    # use changes in velocities to find note on / note off events
    velocity_changes = np.nonzero(np.diff(piano_roll).T)

    # keep track on velocities and note on times
    prev_velocities = np.zeros(notes, dtype=int)
    note_on_time = np.zeros(notes)

    for time, note in zip(*velocity_changes):
        # use time + 1 because of padding above
        velocity = piano_roll[note, time + 1]
        time = time / fs
        if velocity > 0:
            if prev_velocities[note] == 0:
                note_on_time[note] = time
                prev_velocities[note] = velocity
        else:
            pm_note = pretty_midi.Note(
                velocity=prev_velocities[note],
                pitch=note,
                start=note_on_time[note],
                end=time)
            instrument.notes.append(pm_note)
            prev_velocities[note] = 0
    pm.instruments.append(instrument)
    return pm


class LSTMAutoencoder:

    def __init__(self, num_hidden_states, num_unroll, num_dense_units, learning_rate, weight_decay):

        '''
        :param num_layers: Number of layers in the LSTM Network
        :param num_hidden_states: Number of hidden units per layer. Must be a tuple or a list.
        :param num_unroll: Amount to unroll the network by

        '''

        #Initialize hyperparameters
        self.num_hidden_states = num_hidden_states
        self.num_unroll = num_unroll
        self.num_dense_units = num_dense_units
        self.learning_rate = learning_rate
        self.weight_decay = weight_decay
        self.sampling_rate = None

        #Initialize placeholders for input
        self.input_sequence = tf.placeholder(tf.float32, [None, num_unroll, 128], name ='input_sequence')
        self.input_sequence_content = tf.placeholder(tf.float32, [None, num_unroll, 128], name ='input_sequence_content')
        self.input_sequence_style = tf.placeholder(tf.float32, [None, num_unroll, 128], name ='input_sequence_style')
        self.input_state = [tf.placeholder(tf.float32, [None, i], name ='input_state_' + str(i)) for i in self.num_hidden_states]
        self.input_decoder = tf.placeholder(tf.float32, [None, num_unroll, 128], name ='input_decoder')

        #Initialize variables for output
        self.train_autoencoder = None
        self.train_input_opt = None
        self.prediction = None
        self.decoder_generation = None
        self.train_loss = None
        self.opt_loss = None
        self.encoder_state = None

        #Initialize training set
        self.training_set = None

        #Initialize session parameters
        self.saver = None
        self.sess = None

        self.temp = None


    def build_model(self):
        '''
        Builds LSTM Autoencoder model
        :return: None
        '''
        print("Constructing Graph")
        input_opt_var = tf.Variable(tf.truncated_normal(shape = [1, self.num_unroll, 128]), name = 'input_opt_var')
        input_opt = tf.nn.sigmoid(input_opt_var)

        with tf.variable_scope('encoder'):
            enc_lstm_cells = tf.contrib.rnn.MultiRNNCell(([tf.contrib.cudnn_rnn.CudnnCompatibleLSTMCell(i) for i in self.num_hidden_states]))
            encoder_output, self.encoder_state = tf.nn.dynamic_rnn(enc_lstm_cells, self.input_sequence, dtype=tf.float32)
            content_output, content_state = tf.nn.dynamic_rnn(enc_lstm_cells, self.input_sequence_content, dtype=tf.float32)
            style_output, style_state = tf.nn.dynamic_rnn(enc_lstm_cells, self.input_sequence_style, dtype=tf.float32)
            opt_output, opt_state = tf.nn.dynamic_rnn(enc_lstm_cells, input_opt, dtype=tf.float32)

        with tf.variable_scope('decoder'):
            dec_lstm_cells = tf.contrib.rnn.MultiRNNCell(([tf.contrib.cudnn_rnn.CudnnCompatibleLSTMCell(i) for i in self.num_hidden_states[::-1]]))
            decoder_output, decoder_state = tf.nn.dynamic_rnn(dec_lstm_cells, self.input_decoder, dtype=tf.float32, initial_state = self.encoder_state[::-1])
            # decoder_generate_output, decoder_generate_state = tf.nn.dynamic_rnn(dec_lstm_cells, self.input_decoder, dtype=tf.float32, initial_state = (tf.nn.rnn_cell.LSTMStateTuple(self.input_state[1], content_state[1][1]),tf.nn.rnn_cell.LSTMStateTuple(content_state[0][0], content_state[0][1])))
            decoder_generate_output, decoder_generate_state = tf.nn.dynamic_rnn(dec_lstm_cells, self.input_decoder, dtype=tf.float32, initial_state = self.encoder_state[::-1])

        with tf.variable_scope('dense'):
            W1 = tf.Variable(tf.truncated_normal(shape=[self.num_hidden_states[0], self.num_dense_units]), name='weights1')
            b1 = tf.Variable(tf.truncated_normal(shape=[self.num_dense_units]), name='biases1')

            W2 = tf.Variable(tf.truncated_normal(shape=[self.num_dense_units, 128]), name='weights2')
            b2 = tf.Variable(tf.truncated_normal(shape=[1, 128]), name='biases2')

        output = []
        self.prediction = []
        self.decoder_generation = []

        output_unstack = tf.unstack(decoder_output, self.num_unroll, 1)
        output_generate_unstack = tf.unstack(decoder_generate_output, self.num_unroll, 1)
        for i in range(self.num_unroll):
            output.append(tf.matmul(tf.nn.relu(tf.matmul(output_unstack[i], W1) + b1), W2) + b2)
            self.prediction.append(tf.nn.sigmoid(tf.matmul(tf.nn.relu(tf.matmul(output_unstack[i], W1) + b1), W2) + b2))
            self.decoder_generation.append(tf.nn.sigmoid(tf.matmul(tf.nn.relu(tf.matmul(output_generate_unstack[i], W1) + b1), W2) + b2))

        self.temp = tf.unstack(self.input_sequence, self.num_unroll, 1)[::-1]
        cross_entropy_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=output, labels= tf.unstack(self.input_sequence, self.num_unroll, 1)[::-1]))
        self.train_loss = cross_entropy_loss + self.weight_decay * (tf.nn.l2_loss(W2) + tf.nn.l2_loss(W1))
        self.train_autoencoder = tf.train.AdamOptimizer(self.learning_rate).minimize(self.train_loss)

        style_diff = tf.nn.l2_loss(content_state[0][0] - opt_state[0][0]) + \
                     tf.nn.l2_loss(content_state[0][1] - opt_state[0][1]) + \
                     tf.nn.l2_loss(tf.matmul(tf.transpose(self.input_state[1]), self.input_state[1]) - \
                                   tf.matmul(tf.transpose(opt_state[1][0]), opt_state[1][0])) / (4 * 900 * 900) + \
                     tf.nn.l2_loss(tf.matmul(tf.transpose(content_state[1][1]), content_state[1][1]) - \
                                     tf.matmul(tf.transpose(opt_state[1][1]), opt_state[1][1])) / (4 * 900 * 900)
        self.opt_loss = tf.reduce_mean(tf.nn.l2_loss(style_diff)) + 200 * tf.nn.l2_loss(input_opt)
        self.train_input_opt = tf.train.AdamOptimizer(self.learning_rate).minimize(self.opt_loss, var_list=[input_opt_var])

    def load_training_set(self, paths, sampling_rate):
        '''
        :param paths: List of paths to .mid files
        :return: None
        '''
        print("Loading training data set")
        for path in paths:
            for root, dirs, files in os.walk(path):
                for file in files:
                    if file[-3:] == "mid":
                        try:
                            fn = os.path.join(root, file)
                            midi_data = pretty_midi.PrettyMIDI(fn)
                            roll = midi_data.get_piano_roll(sampling_rate)
                            song = np.clip(roll, 0, 1)
                            if self.training_set is None:
                                self.training_set = song
                            else:
                                self.training_set = np.hstack((self.training_set, song))
                        except:
                            pass

        self.training_set = self.training_set.T
        self.sampling_rate = sampling_rate

    def init_vars(self, load_model = None):
        '''
        :param load_model: Path for model to be loaded.
        :return: None
        '''
        print("Initializing variables")
        init = tf.initialize_all_variables()
        self.sess = tf.InteractiveSession()
        self.saver = tf.train.Saver()

        if load_model:
            self.saver.restore(self.sess, load_model)
        else:
            self.sess.run(init)

    def train_model(self, num_epochs, batch_size, save_model = None):
        print("Training Model")
        for epoch in range(num_epochs):
            for batch in range(batch_size):

                input_batch = []
                for i in range(batches):
                    ran = np.random.randint(0, len(self.training_set) - self.num_unroll)
                    input_batch.append(self.training_set[ran: ran + self.num_unroll])
                feed = {self.input_sequence: input_batch,
                         self.input_sequence_content: np.zeros(np.array(input_batch).shape),
                         self.input_sequence_style: np.zeros(np.array(input_batch).shape),
                         self.input_decoder: np.zeros(np.array(input_batch).shape)}
                feed.update({i: d for i, d in zip(self.input_state, [np.zeros([batches, j]) for j in self.num_hidden_states])})

                pred, loss, train, temp = self.sess.run([self.prediction, self.train_loss, self.train_autoencoder, self.temp], feed_dict=feed)

            print("Epoch: " + str(epoch) + " Loss: " + str(loss))

            if save_model:
                 self.saver.save(self.sess, save_model)

    def output_generate(self, content_song, style_song, threshold):
        midi_data = pretty_midi.PrettyMIDI(content_song)
        roll = midi_data.get_piano_roll(self.sampling_rate)
        content = np.clip(roll, 0, 1).T
        content = np.vstack((content, np.zeros([len(content) % self.num_unroll, 128])))

        midi_data = pretty_midi.PrettyMIDI(style_song)
        roll = midi_data.get_piano_roll(self.sampling_rate)
        style = np.clip(roll, 0, 1).T
        style = np.vstack((style, np.zeros([len(style) % self.num_unroll, 128])))

        style_state = np.zeros([1, self.num_hidden_states[1]])
        for i in range(int(len(style)/self.num_unroll)):

            input_batch = []
            input_batch.append(style[self.num_unroll * i: (i + 1) * self.num_unroll])
            feed = {self.input_sequence: input_batch,
                     self.input_sequence_content: np.zeros(np.array(input_batch).shape),
                     self.input_sequence_style: np.zeros(np.array(input_batch).shape),
                     self.input_decoder: np.zeros(np.array(input_batch).shape)}
            feed.update({i: d for i, d in zip(self.input_state, [np.zeros([batches, j]) for j in self.num_hidden_states])})

            style_state += self.sess.run([self.encoder_state], feed_dict=feed) / (int(len(style)/self.num_unroll))
        output_song = []
        for i in range(int(len(content)/self.num_unroll)):

            input_content = []
            input_content.append(content[self.num_unroll * i: (i + 1) * self.num_unroll])
            feed = {self.input_sequence: np.zeros(np.array(input_content).shape),
                     self.input_sequence_content: input_content,
                     self.input_sequence_style: np.zeros(np.array(input_content).shape),
                     self.input_decoder: np.zeros(np.array(input_batch).shape)}
            feed.update({i: d for i, d in zip(self.input_state, [np.zeros([batches, j]) for j in self.num_hidden_states])})

            decoder_generation = self.sess.run([self.decoder_generation], feed_dict=feed)
            output_song.append(np.array(decoder_generation).squeeze())

        output_song = np.concatenate(tuple(output_song))
        output_song[output_song > threshold] = 1
        output_song[output_song <= threshold] = 0

        pm = piano_roll_to_pretty_midi(output_song.T * 100, fs = self.sampling_rate)
        pm.write(content_song[:-4] + "_generate.mid")

    def output_reproduce(self, content_path, threshold, sampling_rate = None):
        print("Reproducing songs")

        if sampling_rate:
            self.sampling_rate = sampling_rate

        for root, dirs, files in os.walk(content_path):
            for song in files:
                if "reproduce" not in song:
                    content_song = os.path.join(root, song)
                    midi_data = pretty_midi.PrettyMIDI(content_song)
                    roll = midi_data.get_piano_roll(self.sampling_rate)
                    content = np.clip(roll, 0, 1).T
                    content = np.vstack((content, np.zeros([len(content) % self.num_unroll, 128])))
                    output_song = []

                    for i in range(int(len(content)/self.num_unroll)):

                        input_batch = []
                        input_batch.append(content[self.num_unroll * i: (i + 1) * self.num_unroll])
                        feed = {self.input_sequence: input_batch,
                                 self.input_sequence_content: np.zeros(np.array(input_batch).shape),
                                 self.input_sequence_style: np.zeros(np.array(input_batch).shape),
                                 self.input_decoder: np.zeros(np.array(input_batch).shape)}
                        feed.update({i: d for i, d in zip(self.input_state, [np.zeros([1, j]) for j in self.num_hidden_states])})

                        prediction = self.sess.run([self.prediction], feed_dict=feed)
                        output_song.append(np.array(prediction).squeeze())

                    output_song = np.concatenate(tuple(output_song))
                    output_song[output_song > threshold] = 1
                    output_song[output_song <= threshold] = 0

                    pm = piano_roll_to_pretty_midi(output_song.T * 100, fs = self.sampling_rate)
                    pm.write(content_song[:-4] + "_reproduce.mid")
      

        

if __name__ == "__main__":
    lstm = LSTMAutoencoder([300, 300, 300, 300, 300, 300, 200, 200, 200, 200, 200, 200, 200, 200, 100, 100, 100, 100, 100, 100, 100, 100], 60, 200, 0.01, 0.00001)
    lstm.build_model()
    # lstm.init_vars(sys.argv[2])
    lstm.init_vars()

    lstm.load_training_set([sys.argv[1]], 20)
    lstm.train_model(100, 500, sys.argv[2])
    lstm.output_reproduce(sys.argv[3], 0.3, 20)
    
